from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoFormItem


def todo_list_list(request):
    table = TodoList.objects.all()
    context = {
        "todo_list_list": table,
    }
    return render(request, "todos/main-page.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": todo_item
    }
    return render(request, "todos/detail.html", context)
